const items = [
    ["001", "Keyboard Logitek", 60000, "Keyboard yang mantap untuk kantoran", "logitek.jpg"], 
    ["002", "Keyboard MSI", 300000, "Keyboard gaming MSI mekanik", "msi.jpg"],
    ["003", "Mouse Genius", 50000, "Mouse Genius biar lebih pinter", "genius.jpeg"],
    ["004", "Mouse Jerry", 30000, "Mouse yang disukai kucing", "jerry.jpg"]
];

window.addEventListener("load", () => {
    const searchForm = document.getElementById("formItem");
    searchForm.addEventListener("submit", (e) => {
        e.preventDefault();
        recreate(e.target[0].value);
    });

    const searchInput = document.getElementById("keyword");
    searchInput.addEventListener("input", (e) => {
        recreate(e.target.value);
    });
});

function recreate(keyword) {
    const itemContainer = document.getElementById("listBarang");
    itemContainer.innerHTML = "";
    items.forEach((item) => {
        if (item[1].toLowerCase().includes(keyword.toLowerCase())) {
            itemContainer.innerHTML += createTemplate(item);
        }
    });

    const cards = document.querySelectorAll(".card");
    for (const card of [...cards]) {
        const btn = card.querySelector("#addCart");
        btn.addEventListener("click", () => {
            const cart = document.getElementById("cart");
            const text = cart.innerText;
            const count = parseInt(text.substring(text.indexOf("(") + 1, text.indexOf(")")));
            cart.innerHTML = "";
            if (count >= 9) {
                cart.innerHTML = `<i class="fas fa-shopping-cart"></i>(9+)`;
            } else {
                cart.innerHTML = `<i class="fas fa-shopping-cart"></i>(${count + 1})`;
            }
        });
    }
}

function createTemplate(item) {
    return `<div class="col-4 mt-2">
        <div class="card" style="width: 18rem;">
            <img src="images/${item[4]}" class="card-img-top" height="200px" width="200px"  alt="...">
            <div class="card-body">
                <h5 class="card-title" id="itemName">${item[1]}</h5>
                <p class="card-text" id="itemDesc">${item[3]}</p>
                <p class="card-text">Rp ${item[2]}</p>
                <a href="#" class="btn btn-primary" id="addCart">Tambahkan ke keranjang</a>
            </div>
        </div>
    </div>`;
}